package `in`.bizmo.mdm.callmanager.helpers

import `in`.bizmo.mdm.callmanager.R
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat

class NotificationHelper {
    object SingletonNotification {
        const val NOTIFICATION_ID = 150
        private const val mdmChannelId = "mdm"

        fun createCallFilteringNotification(context: Context): Notification? {
            val notificationManager = (context.getSystemService(Context.NOTIFICATION_SERVICE)
                    as NotificationManager)
            ensureChannel(notificationManager, mdmChannelId, "Calls filtering")
            val mBuilder: NotificationCompat.Builder? = createBuilder(context,
                "Bizmobile Go! manages your calls")
            return mBuilder!!.build()
        }

        private fun createBuilder(context: Context, contentText: String):
                NotificationCompat.Builder? {
            return NotificationCompat.Builder(context, mdmChannelId)
                .setSmallIcon(R.drawable.company_icon_white)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(contentText)
                .setAutoCancel(true)
        }

        private fun ensureChannel(notificationManager: NotificationManager, channelId: String?,
            channelName: String?, importance: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    channelId,
                    channelName,
                    importance
                )
                notificationManager.createNotificationChannel(channel)
            }
        }

        private fun ensureChannel(notificationManager: NotificationManager, channelId: String?,
                          channelName: String?) {
            ensureChannel(
                notificationManager,
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
        }
    }
}