package `in`.bizmo.mdm.callmanager.helpers

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

class PermissionsHelper {

    object SingletonPermissions {

        fun arePermissionsGranted(
            context: Context?,
            permissions: Array<String>): Boolean {
            for (permission in permissions) {
                if (ContextCompat.checkSelfPermission(context!!, permission) !=
                    PackageManager.PERMISSION_GRANTED
                ) return false
            }
            return true
        }
    }
}