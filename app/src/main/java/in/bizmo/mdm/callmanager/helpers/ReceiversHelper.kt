package `in`.bizmo.mdm.callmanager.helpers

import `in`.bizmo.mdm.callmanager.receivers.CallRestrictionsConfigReceiver
import `in`.bizmo.mdm.callmanager.receivers.OutgoingCallsReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class ReceiversHelper {
    object ReceiverSingleton {
        private var mCallFilteringReceiver: OutgoingCallsReceiver? = null
        private var mCallManagedConfigReceiver: CallRestrictionsConfigReceiver? = null

        fun registerReceivers(context: Context) {
            registerCallFilteringReceiver(context)
        }

        fun unregisterReceivers(context: Context) {
            unregisterCallFilteringReceiver(context)
        }

        fun registerManagedConfigReceiver(context: Context) {
            assert(mCallManagedConfigReceiver == null)
            mCallManagedConfigReceiver = CallRestrictionsConfigReceiver()
            val intentFilter = IntentFilter(Intent.ACTION_APPLICATION_RESTRICTIONS_CHANGED)
            context.registerReceiver(mCallManagedConfigReceiver, intentFilter)
        }

        private fun registerCallFilteringReceiver(context: Context) {
            mCallFilteringReceiver = OutgoingCallsReceiver()
            val intentFilter = IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL)
            context.registerReceiver(mCallFilteringReceiver, intentFilter)
        }

        private fun unregisterCallFilteringReceiver(context: Context) {
            assert(mCallFilteringReceiver != null)
            context.unregisterReceiver(mCallFilteringReceiver)
            mCallFilteringReceiver = null
        }
    }
}