package `in`.bizmo.mdm.callmanager.receivers

import `in`.bizmo.mdm.callmanager.services.FilteringForegroundService
import `in`.bizmo.mdm.callmanager.services.ManagedConfigService
import `in`.bizmo.mdm.callmanager.ui.MainActivity.Companion.filteredNumbersListPreference
import `in`.bizmo.mdm.callmanager.ui.MainActivity.Companion.serviceStatePreference
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.widget.Toast
import androidx.preference.PreferenceManager

class BootCompletedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (Intent.ACTION_BOOT_COMPLETED == intent.action) {
            val appContext = context.applicationContext
        
            Toast.makeText(appContext, "Broadcast receiver boot completed !", Toast.LENGTH_LONG).show()

            // Starts the broadcast receiver for Intent.ACTION_APPLICATION_RESTRICTIONS_CHANGED
            val managedConfigIntent = Intent(appContext, ManagedConfigService::class.java)
            appContext.startService(managedConfigIntent)

            val preferences = PreferenceManager.getDefaultSharedPreferences(appContext)
            val phoneNumbersList = preferences.getString(filteredNumbersListPreference, "")

            Toast.makeText(appContext, "phoneNumbersList = $phoneNumbersList", Toast.LENGTH_LONG).show()

            val startServiceCmd = phoneNumbersList != null && !TextUtils.equals(phoneNumbersList, "")
            val activated = preferences.getBoolean(serviceStatePreference, false)

            // Only start the service if not activated
            if(startServiceCmd && !activated) {
                val intentService = Intent(appContext, FilteringForegroundService::class.java)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    appContext.startForegroundService(intentService)
                } else {
                    appContext.startService(intentService)
                }
            }

//            val phoneNumbersList = PreferenceManager.getDefaultSharedPreferences(appContext)
//                    .getString(DialerActivity.filteredNumbersListPreference, "")
//            val activatedOnKohada = !TextUtils.equals(phoneNumbersList, "")
//            if (activatedOnKohada) {
//                val intentService = Intent(appContext, FilteringForegroundService::class.java)
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    appContext.startForegroundService(intentService)
//                } else {
//                    appContext.startService(intentService)
//                }
//            }
        }
    }
}