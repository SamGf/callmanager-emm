package `in`.bizmo.mdm.callmanager.receivers

import `in`.bizmo.mdm.callmanager.services.FilteringForegroundService
import `in`.bizmo.mdm.callmanager.ui.MainActivity.Companion.filteredNumbersListPreference
import `in`.bizmo.mdm.callmanager.ui.MainActivity.Companion.serviceStatePreference
import android.content.*
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.core.content.ContextCompat.startForegroundService
import androidx.preference.PreferenceManager

class CallRestrictionsConfigReceiver : BroadcastReceiver() {

    private val TAG = CallRestrictionsConfigReceiver::class.java.simpleName

    override fun onReceive(context: Context, intent: Intent) {
        val myRestrictionsMgr = context.getSystemService(Context.RESTRICTIONS_SERVICE)
                as RestrictionsManager
        val appRestrictions :Bundle = myRestrictionsMgr.applicationRestrictions
        val phoneNumbersList :String? = appRestrictions.getString("outcall_whitelist")
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        val stopServiceCmd = phoneNumbersList == null || TextUtils.equals(phoneNumbersList, "")
        val activated = preferences.getBoolean(serviceStatePreference, false)

        // Only stop the service if activated
        if(stopServiceCmd) {
            if(activated) {
                editor.remove(filteredNumbersListPreference)
                stopFilteringJob(context, editor)
            }
        } else {
            editor.putString(filteredNumbersListPreference, phoneNumbersList)
            startFilteringJob(context, editor)
        }
        editor.apply()
    }

    private fun startFilteringJob(context: Context, editor: SharedPreferences.Editor) {
        val intentService = Intent(context, FilteringForegroundService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            editor.putBoolean(serviceStatePreference, true)
            editor.apply()
            startForegroundService(context, intentService)
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(context, intentService)
            } else {
                context.startService(intentService)
            }
        }
        Log.d(TAG,"Calls filtering job has started")
    }

    private fun stopFilteringJob(context: Context, editor: SharedPreferences.Editor) {
        // Setting on Kohada => {OFF,[]}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            editor.putBoolean(serviceStatePreference, false)
            editor.apply()
        }
        val intentService = Intent(context, FilteringForegroundService::class.java)
        context.stopService(intentService)
        Log.d(TAG, "Calls filtering job has stopped")
    }
}