package `in`.bizmo.mdm.callmanager.receivers

import `in`.bizmo.mdm.callmanager.R
import `in`.bizmo.mdm.callmanager.ui.MainActivity.Companion.filteredNumbersListPreference
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.preference.PreferenceManager

class OutgoingCallsReceiver : BroadcastReceiver() {

    private val TAG: String = OutgoingCallsReceiver::class.java.simpleName

    override fun onReceive(context: Context, intent: Intent) {
        if (Intent.ACTION_NEW_OUTGOING_CALL == intent.action) {
            val phoneNumber :String? = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER)
            val phoneNumbersList =
                PreferenceManager.getDefaultSharedPreferences(context)
                    .getString(filteredNumbersListPreference, "")
            val authorized = phoneNumbersList!!.contains(phoneNumber.toString())
            if (!authorized) {
                val errorMessage = TextUtils.concat(
                    context.resources.getString(R.string.outgoing_call_forbidden),
                    " ", phoneNumber).toString()
                denyCall(context, errorMessage)
            } else {
              Log.d(TAG, "Proceed with outgoing call: $phoneNumber")
            }
        }
    }

    private fun denyCall(context: Context, message: String) {
        resultData = null //Canceling call operation
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}