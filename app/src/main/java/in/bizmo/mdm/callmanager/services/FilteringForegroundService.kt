package `in`.bizmo.mdm.callmanager.services

import `in`.bizmo.mdm.callmanager.helpers.NotificationHelper.SingletonNotification.NOTIFICATION_ID
import `in`.bizmo.mdm.callmanager.helpers.NotificationHelper.SingletonNotification.createCallFilteringNotification
import `in`.bizmo.mdm.callmanager.helpers.ReceiversHelper.ReceiverSingleton.registerReceivers
import `in`.bizmo.mdm.callmanager.helpers.ReceiversHelper.ReceiverSingleton.unregisterReceivers
import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder

class FilteringForegroundService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            registerReceivers(this)
        }
        val filteringNotif: Notification? = createCallFilteringNotification(this)
        startForeground(NOTIFICATION_ID, filteringNotif)
    }

    override fun onDestroy() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            unregisterReceivers(this)
        }
    }
}