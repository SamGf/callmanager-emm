package `in`.bizmo.mdm.callmanager.services

import `in`.bizmo.mdm.callmanager.helpers.ReceiversHelper.ReceiverSingleton.registerManagedConfigReceiver
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.widget.Toast

class ManagedConfigService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Toast.makeText(this, "Service which starts the broadcast receiver has started",
            Toast.LENGTH_LONG).show()
        registerManagedConfigReceiver(this)
    }
}