package `in`.bizmo.mdm.callmanager.services

import `in`.bizmo.mdm.callmanager.R
import `in`.bizmo.mdm.callmanager.ui.MainActivity
import `in`.bizmo.mdm.callmanager.ui.MainActivity.Companion.serviceStatePreference
import android.content.Context
import android.net.Uri
import androidx.preference.PreferenceManager
import android.telecom.CallRedirectionService
import android.telecom.PhoneAccountHandle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast

class OutgoingCallsService : CallRedirectionService() {

    private val TAG: String = OutgoingCallsService::class.java.simpleName

    override fun onPlaceCall(handle: Uri, initialPhoneAccount: PhoneAccountHandle,
        allowInteractiveResponse: Boolean) {
        val serviceActivated: Boolean = PreferenceManager.getDefaultSharedPreferences(this)
            .getBoolean(serviceStatePreference, false)
        val phoneNumber = handle.encodedSchemeSpecificPart
        if (serviceActivated) {
            val phoneNumbersList =
                PreferenceManager.getDefaultSharedPreferences(this)
                    .getString(MainActivity.filteredNumbersListPreference, "")
            val authorized = phoneNumbersList!!.contains(phoneNumber.toString())
            if (!authorized) {
                val text = resources.getString(R.string.outgoing_call_forbidden)
                val errorMessage = TextUtils.concat(text, " ", phoneNumber).toString()
                denyCall(this, errorMessage)
                return
            }
        }

        Log.d(TAG, "Proceed with outgoing call: $phoneNumber")
        placeCallUnmodified()
    }

    private fun denyCall(context: Context, message: String) {
        cancelCall()
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}