package `in`.bizmo.mdm.callmanager.ui

import `in`.bizmo.mdm.callmanager.R
import `in`.bizmo.mdm.callmanager.databinding.ActivityMainBinding
import `in`.bizmo.mdm.callmanager.helpers.PermissionsHelper.SingletonPermissions.arePermissionsGranted
import `in`.bizmo.mdm.callmanager.helpers.ReceiversHelper.ReceiverSingleton.registerManagedConfigReceiver
import android.Manifest.permission
import android.app.role.RoleManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager

class MainActivity : AppCompatActivity() {
    companion object {
        val TAG = MainActivity::class.java.simpleName
        const val serviceStatePreference = "serviceState"
        const val filteredNumbersListPreference = "filteredNumbersList"
    }

    private lateinit var phoneNumbersListView: TextView
    private val REQUEST_REDIRECTION_ROLE = 1
    private val REQUEST_PERMISSIONS = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        val view: View = binding.root
        setContentView(view)

        phoneNumbersListView = binding.phoneNumberslistTextView
    }

    override fun onStart() {
        super.onStart()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            promptForRole(RoleManager.ROLE_CALL_REDIRECTION)
        } else {
            checkPermissions()
        }
        registerManagedConfigReceiver(this)
    }

    override fun onResume() {
        super.onResume()
        val preferenceManager = PreferenceManager.getDefaultSharedPreferences(this)
        var phoneNumbersList = preferenceManager.getString(filteredNumbersListPreference, "")

        // All numbers are whitelisted
        if(TextUtils.equals(phoneNumbersList, "")) {
            phoneNumbersListView.text = resources.getText(R.string.all_numbers_whitelisted)

        } else {
            phoneNumbersList = phoneNumbersList!!.replace(",", "\n\n", true)
            phoneNumbersListView.text = phoneNumbersList
        }
    }

    private fun checkPermissions() {
        val mPermissions = arrayOf(
            permission.READ_CALL_LOG,
            permission.PROCESS_OUTGOING_CALLS
        )

        if (!arePermissionsGranted(this, mPermissions)) {
            ActivityCompat.requestPermissions(this, mPermissions, REQUEST_PERMISSIONS)
        }
    }

    private fun promptForRole(roleName: String) {
        val roleManager = getSystemService(ROLE_SERVICE) as RoleManager
        val isRoleHeld = roleManager.isRoleHeld(roleName)
        if (!isRoleHeld) {
            val intent = roleManager.createRequestRoleIntent(roleName)
            when (roleName) {
                RoleManager.ROLE_CALL_REDIRECTION ->
                    startActivityForResult(intent,
                        REQUEST_REDIRECTION_ROLE
                    )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            RESULT_OK -> Log.d(TAG,"User accepted the request code: $requestCode")
            RESULT_CANCELED -> {
                when(requestCode) {
                    REQUEST_REDIRECTION_ROLE -> promptForRole(RoleManager.ROLE_CALL_REDIRECTION)
                }
            } else -> {
                finishAffinity()
                Log.d(TAG,"Unexpected result code $resultCode")
                Toast.makeText(this, "An expected error while initializing " +
                        "the application", Toast.LENGTH_LONG).show()
            }
        }
    }
}
